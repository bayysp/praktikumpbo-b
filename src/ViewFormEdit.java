import javax.swing.*;
import java.awt.*;

public class ViewFormEdit extends JFrame {
    JLabel lNim = new JLabel("NIM    :");
    JTextField tfNim = new JTextField();
    JLabel lNamaMhs = new JLabel("Nama Mahasiswa   :");
    JTextField tfNamaMhs = new JTextField();
    JLabel lAlamatMhs = new JLabel("Alamat Mahasiswa  :");
    JTextField tfAlamatMhs = new JTextField();
    JButton btnEditPanel = new JButton("Edit");
    JButton btnBatalPanel = new JButton("Batal");

    public ViewFormEdit() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setLayout(null);
        setSize(600, 500);

        add(lNim);
        lNim.setBounds(5, 5, 90, 20);
        add(tfNim);
        tfNim.setBounds(110, 5, 120, 20);
        add(lNamaMhs);
        lNamaMhs.setBounds(5, 30, 90, 20);
        add(tfNamaMhs);
        tfNamaMhs.setBounds(110, 30, 120, 20);
        add(lAlamatMhs);
        lAlamatMhs.setBounds(5, 55, 90, 20);
        add(tfAlamatMhs);
        tfAlamatMhs.setBounds(110, 55, 120, 20);
        add(btnEditPanel);
        btnEditPanel.setBounds(20, 105, 90, 20);
        add(btnBatalPanel);
        btnBatalPanel.setBounds(130, 105, 90, 20);

    }
    public String getNim(){
        return tfNim.getText();
    }
    public String getNama(){
        return tfNamaMhs.getText();
    }
    public String getAlamat(){
        return tfAlamatMhs.getText();
    }
}
