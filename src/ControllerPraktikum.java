import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ControllerPraktikum {
    //perantara model dan view
    ModelPraktikum modelPraktikum;
    ViewPraktikum viewPraktikum;
    ViewFormEdit viewFormEdit;
    String dataterpilih = null;
    int baris,kolom;



    public ControllerPraktikum(ModelPraktikum modelPraktikum, ViewPraktikum viewPraktikum) {
        this.modelPraktikum = modelPraktikum;
        this.viewPraktikum = viewPraktikum;

        if (modelPraktikum.getBanyakData() != 0) { //kalau banyak datanya tidak sama dengan 0
            updateTable();
            //menampilkan data yang ada didalam database ke tabel
        } else {
            JOptionPane.showMessageDialog(null, "Data Tidak Ada");
        }

        viewPraktikum.btnTambahPanel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (viewPraktikum.getNim().equals("")
                        || viewPraktikum.getNama().equals("")
                        || viewPraktikum.getAlamat().equals("")) {
                    JOptionPane.showMessageDialog(null, "Field tidak boleh kosong");
                } else {
                    String nim = viewPraktikum.getNim();
                    String nama = viewPraktikum.getNama();
                    String alamat = viewPraktikum.getAlamat();

                    modelPraktikum.insertMahasiswa(nim, nama, alamat);
                    viewPraktikum.tfNim.setText("");
                    viewPraktikum.tfNamaMhs.setText("");
                    viewPraktikum.tfAlamatMhs.setText("");

                    //untuk menampilkan output langsung tanpa reload
                    updateTable();
                }
            }
        });

        viewPraktikum.tabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) { //alt+insert
                baris = viewPraktikum.tabel.getSelectedRow();
                kolom = viewPraktikum.tabel.getSelectedColumn(); //ga kepake
                //ngambil nim yang ada di kolom indeks 0
                dataterpilih = viewPraktikum.tabel.getValueAt(baris, 0).toString();

                System.out.println(dataterpilih);
                if (dataterpilih != null){ //jika ada data yang terpilih
                    viewPraktikum.btnHapus.setEnabled(true);
                    viewPraktikum.btnEdit.setEnabled(true);
                    //set supaya btnHapus jadi bisa diklik
                }
            }
        });

        viewPraktikum.btnHapus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (dataterpilih != null){
                        //memanggil method deleteMahasiswa di modelPraktikum dengan mengirim parameter dataterpilih
                        modelPraktikum.deleteMahasiswa(dataterpilih);
                        updateTable();
                        viewPraktikum.btnHapus.setEnabled(false);
                    }
                }catch (Exception ex){
                    System.out.println(ex.getMessage());
                    System.out.println("Gagal Hapus");
                }
            }
        });


        viewPraktikum.btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    if(dataterpilih!=null){
                        String dataEditNim = viewPraktikum.tabel.getValueAt(baris,0).toString();
                        String dataEditNama = viewPraktikum.tabel.getValueAt(baris,1).toString();
                        String dataEditAlamat = viewPraktikum.tabel.getValueAt(baris,2).toString();

                        System.out.println("data terpilih edit : "+dataEditNim+" "+dataEditNama+" "+dataEditAlamat);

                        viewFormEdit = new ViewFormEdit();

                        viewPraktikum.dispose();

                        viewFormEdit.tfNim.setText(dataEditNim);
                        viewFormEdit.tfNamaMhs.setText(dataEditNama);
                        viewFormEdit.tfAlamatMhs.setText(dataEditAlamat);
                        viewFormEdit.tfNim.setEditable(false);

                        viewFormEdit.btnEditPanel.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                if (dataterpilih != null){
                                    modelPraktikum.updateMahasiswa(
                                            viewFormEdit.getNim(),
                                            viewFormEdit.getNama(),
                                            viewFormEdit.getAlamat());
                                    viewFormEdit.dispose();
                                    MVC_Praktikum mvc_praktikum = new MVC_Praktikum();
                                }
                            }
                        });
                    }
                }catch(Exception ex){
                    System.out.println(ex.getMessage());
                    System.out.println("Edit Gagal");
                }
            }
        });

    }



    private void updateTable() { //update data tanpa reload
        String dataMahasiswa[][] = modelPraktikum.readMahasiswa();
        viewPraktikum.tabel.setModel(new JTable(dataMahasiswa, viewPraktikum.namaKolom).getModel());
    }
}
